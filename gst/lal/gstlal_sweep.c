/*
 * Copyright (C) 2022  Aaron Viets <aaron.viets@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/*
 * ========================================================================
 *
 *				  Preamble
 *
 * ========================================================================
 */


/*
 * stuff from the C library
 */


#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>


/*
 * stuff from glib/gstreamer
 */


#include <glib.h>
#include <glib/gprintf.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/audio/audio.h>
#include <gst/audio/audio-format.h>


/*
 * our own stuff
 */


#include <gstlal/gstlal.h>
#include <gstlal/gstlal_audio_info.h>
#include <gstlal/gstlal_debug.h>
#include <gstlal_firtools.h>
#include <gstlal_sweep.h>


/*
 * ============================================================================
 *
 *			   GStreamer Boilerplate
 *
 * ============================================================================
 */


#define GST_CAT_DEFAULT gstlal_sweep_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);


G_DEFINE_TYPE_WITH_CODE(
	GSTLALSweep,
	gstlal_sweep,
	GST_TYPE_BASE_SINK,
	GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "lal_sweep", 0, "lal_sweep element")
);


enum property {
	ARG_FREQUENCY = 1,
	ARG_TF_FREQS,
	ARG_TRANSFER_FUNCTION,
	ARG_TF_UNCERTAINTY,
	ARG_FILENAME,
	ARG_FAKE
};


static GParamSpec *properties[ARG_FAKE];


/*
 * ============================================================================
 *
 *				  Utilities
 *
 * ============================================================================
 */


static void write_tf(double *freqs, complex double *tf, double *tf_unc, guint64 rows, char *filename) {

	guint64 i;
	FILE *fp;
	fp = fopen(filename, "a");

	for(i = 0; i < rows; i++)
		g_fprintf(fp, "%1.7e\t%1.15e\t%1.15e\t%1.7e\n", freqs[i], creal(tf[i]), cimag(tf[i]), tf_unc[i]);

	g_fprintf(fp, "\n\n\n");

	fclose(fp);

	return;
}


static double find_n_eff(double *win, gint64 win_length, gint64 overlap, gint64 num_win, gboolean use_median) {

	if(num_win < 2 || overlap <= 0)
		return (double) num_win;

	double n_eff, num, den, numsqrt;
	gint64 i, j, k, num_overlap, num_plateau, stride = win_length - overlap;

	/* Start by finding how much greater the uncertainty would be if the windows were only one sample long */
	num = den = 0.0;
	/* Relative variance decreases by (sum of squares) / (square of sum), which gives 1/n_eff for the window */
	for(i = 0; i < win_length; i++) {
		num += win[i] * win[i];
		den += win[i];
	}
	/* factor by which the variance of one sample is greater than the variance of a result from window */
	n_eff = num / den / den;

	/* Now, how much is the relative variance reduced by all windows combined? */
	num = den = 0.0;
	/* Maximum number of windows overlapping at any point */
	num_overlap = (win_length + stride - 1) / stride;
	num_overlap = num_win < num_overlap ? num_win : num_overlap;
	/* Start at one end */
	for(i = 0; i < num_overlap - 1; i++) {
		for(j = 0; j < stride; j++) {
			numsqrt = 0.0;
			for(k = 0; k <= i; k++) {
				numsqrt += win[j + k * stride];
				den += (j + i * stride < overlap ? 2 : 1) * win[j + k * stride];
			}
			/* Prefactor accounts for end samples */
			num += (j + i * stride < overlap ? 2 : 1) * numsqrt * numsqrt;
		}
	}
	/* Middle "plateau", where num_overlap windows are overlapped at first, but one window may end during a stride */
	num_plateau = num_win - num_overlap + 1;
	num_plateau = num_plateau > 0 ? num_plateau : 0;
	for(j = 0; j < win_length - (num_overlap - 1) * stride; j++) {
		numsqrt = 0.0;
		for(k = 0; k < num_overlap; k++) {
			numsqrt += win[j + k * stride];
			den += num_plateau * win[j + k * stride];
		}
		num += num_plateau * numsqrt * numsqrt;
	}
	for(j = win_length - (num_overlap - 1) * stride; j < stride; j++) {
		numsqrt = 0.0;
		for(k = 0; k < num_overlap - 1; k++) {
			numsqrt += win[j + k * stride];
			den += num_plateau * win[j + k * stride];
		}
		num += num_plateau * numsqrt * numsqrt;
	}

	n_eff *= den * den / num;

	/* If we use a median, uncertainty is reduced by sqrt(2n/pi), assuming a normal distribution, not by sqrt(n) */
	if(use_median)
		n_eff *= 2.0 / M_PI;

	return n_eff;
}



static void compute_tf(complex double *num, complex double *den, guint64 ndata, int rate, double freq, complex double *tf, double *tf_unc) {

	double smallest, *win, n_eff, coh, psdx, psdy;
	guint64 nwin, intmin, i, j, stride, navg;
	complex double numsum, densum, csd;

	/* 
	 * Compute an appropriate Blackman window to low-pass filter the data.
	 * There are 2 factors to consider:
	 * 1. The length of the data set.  A longer window can be used for better
	 *    quality in long data sets, but we wouldn't want to lose too much data
	 *    from a short data set.
	 * 2. Due to the multiplication of the data by a local oscillator, a loud
	 *    oscillation exists at 2f, which we must destroy.  To do this, we must
	 *    place 2f between side lobes.  In other words, the duration of the
	 *    window should be close to an integer (at least 3) multiple of 1/(2f).
	 */

	nwin = ndata / 8;
	if(nwin > 20 * (guint64) rate)
		nwin = 20 * rate;

	intmin = (guint64) (nwin * 2 * freq / rate);
	if(intmin < 3) {
		nwin = (guint64) (0.5 + 3 * rate / (2 * freq));
		if(nwin > ndata)
			nwin = ndata;
	} else {
		smallest = fabs(nwin * 2 * freq / rate - (guint64) (0.5 + nwin * 2 * freq / rate));
		for(i = intmin; i < 2 * intmin; i++) {
			if(fabs((guint64) (i * rate / freq / 2) * 2 * freq / rate - (guint64) (0.5 + (guint64) (i * rate / freq / 2) * 2 * freq / rate)) < smallest) {
				nwin = (guint64) (i * rate / freq / 2);
				smallest = fabs(nwin * 2 * freq / rate - (guint64) (0.5 + nwin * 2 * freq / rate));
			}
			if(fabs((guint64) (i * rate / freq / 2 + 1) * 2 * freq / rate - (guint64) (0.5 + (guint64) (i * rate / freq / 2 + 1) * 2 * freq / rate)) < smallest) {
				nwin = (guint64) (i * rate / freq / 2 + 1);
				smallest = fabs(nwin * 2 * freq / rate - (guint64) (0.5 + nwin * 2 * freq / rate));
			}
		}
	}

	/* Finally, make the window */
	win = blackman_double(nwin, NULL, FALSE);

	/* Compute the transfer function */
	stride = nwin / 4;
	navg = 1 + (ndata - nwin) / stride;
	while(1 + (ndata - nwin) / (stride + 1) == navg)
		stride++;

	*tf = 0.0;
	/* Cross-spectral densities and power spectral densities needed for coherence and uncertainty calculation */
	csd = 0.0;
	psdx = 0.0;
	psdy = 0.0;
	for(i = 0; i < navg; i++) {
		numsum = densum = 0.0;
		for(j = 0; j < nwin; j++) {
			numsum += win[j] * num[i * stride + j];
			densum += win[j] * den[i * stride + j];
		}
		*tf += numsum / densum;
		csd += conj(numsum) * densum;
		psdx += cabs(numsum) * cabs(numsum);
		psdy += cabs(densum) * cabs(densum);
	}
	/* Normalize */
	*tf /= navg;

	/* Compute uncertainty */
	n_eff = find_n_eff(win, (gint64) nwin, (gint64) (nwin - stride), (gint64) navg, FALSE);
	coh = (cabs(csd) * cabs(csd)) / (psdx * psdy);
	*tf_unc = sqrt((1.0 - coh) / (2.0 * n_eff * coh));

	return;
}


/*
 * ============================================================================
 *
 *			    GstBaseSink Overrides
 *
 * ============================================================================
 */


/*
 * get_unit_size()
 */


static gboolean get_unit_size(GstBaseSink *sink, GstCaps *caps, gsize *size) {

	GstAudioInfo info;
	gboolean success = gstlal_audio_info_from_caps(&info, caps);
	if(success)
		*size = GST_AUDIO_INFO_BPF(&info);
	else
		GST_WARNING_OBJECT(sink, "unable to parse caps %" GST_PTR_FORMAT, caps);
	return success;
}


/*
 * set_caps()
 */


static gboolean set_caps(GstBaseSink *sink, GstCaps *caps) {

	GSTLALSweep *element = GSTLAL_SWEEP(sink);

	gboolean success = TRUE;

	gsize unit_size;

	/* Parse the caps to find the format, sample rate, and number of channels */
	GstStructure *str = gst_caps_get_structure(caps, 0);
	success &= gst_structure_get_int(str, "rate", &element->rate);

	/* Find unit size */
	success &= get_unit_size(sink, caps, &unit_size);
	element->unit_size = unit_size;

	/* If we are writing output to file, and a file already exists with the same name, remove it */
	if(element->filename)
		remove(element->filename);

	/*
	 * Memory allocation
	 */

	/* Start with 20 seconds worth of data storage, reallocate later if more is needed */
	element->max_ndata = 20 * element->rate;
	element->numerator_data = g_malloc(element->max_ndata * sizeof(complex double));
	element->denominator_data = g_malloc(element->max_ndata * sizeof(complex double));

	/* Start with 50 frequency points in the sweep, reallocate later if more are needed */
	element->max_nfreqs = 50;
	element->tf_freqs = g_malloc(element->max_nfreqs * sizeof(double));
	element->transfer_function = g_malloc(element->max_nfreqs * sizeof(complex double));
	element->tf_uncertainty = g_malloc(element->max_nfreqs * sizeof(double));

	return success;
}


/*
 * render()
 */


static GstFlowReturn render(GstBaseSink *sink, GstBuffer *buffer) {

	GSTLALSweep *element = GSTLAL_SWEEP(sink);
	GstMapInfo mapinfo;
	GstFlowReturn result = GST_FLOW_OK;
	guint64 i;

	gst_buffer_map(buffer, &mapinfo, GST_MAP_READ);

	/*
	 * check for discontinuity
	 */

	if(G_UNLIKELY(GST_BUFFER_IS_DISCONT(buffer) || GST_BUFFER_OFFSET(buffer) != element->next_in_offset || !GST_CLOCK_TIME_IS_VALID(element->t0))) {
		element->t0 = GST_BUFFER_PTS(buffer);
		element->offset0 = GST_BUFFER_OFFSET(buffer);
		element->ndata = 0;
	}
	element->next_in_offset = GST_BUFFER_OFFSET_END(buffer);
	GST_DEBUG_OBJECT(element, "have buffer spanning %" GST_BUFFER_BOUNDARIES_FORMAT, GST_BUFFER_BOUNDARIES_ARGS(buffer));

	/* Sync timestamps for frequency property that we want to be controlled	*/
	gst_object_sync_values(GST_OBJECT(sink), GST_BUFFER_PTS(buffer));

	/* Do we need to process stored data? */
	if(element->ndata > 0 && (element->tf_freqs[element->nfreqs - 1] != element->frequency || (GST_BUFFER_FLAG_IS_SET(buffer, GST_BUFFER_FLAG_GAP) && mapinfo.size))) {
		compute_tf(element->numerator_data, element->denominator_data, element->ndata, element->rate, element->tf_freqs[element->nfreqs - 1], element->transfer_function + (element->nfreqs - 1), element->tf_uncertainty + (element->nfreqs - 1));
		element->ndata = 0;
	}

	/* Deal with data on this buffer */
	if(!GST_BUFFER_FLAG_IS_SET(buffer, GST_BUFFER_FLAG_GAP) && mapinfo.size && element->frequency > 0) {

		/* Will we need more memory? */
		complex double *temp;
		double *tempf;
		if(element->ndata + mapinfo.size / element->unit_size > element->max_ndata) {
			element->max_ndata += 20 * element->rate;
			temp = g_malloc(element->max_ndata * sizeof(complex double));
			for(i = 0; i < element->ndata; i++)
				temp[i] = element->numerator_data[i];
			g_free(element->numerator_data);
			element->numerator_data = temp;
			temp = g_malloc(element->max_ndata * sizeof(complex double));
			for(i = 0; i < element->ndata; i++)
				temp[i] = element->denominator_data[i];
			g_free(element->denominator_data);
			element->denominator_data = temp;
		}
		if(element->frequency != element->tf_freqs[element->nfreqs - 1]) {
			if(element->nfreqs + 1 > element->max_nfreqs) {
				element->max_nfreqs += 50;
				tempf = g_malloc(element->max_nfreqs * sizeof(double));
				for(i = 0; i < element->nfreqs; i++)
					tempf[i] = element->tf_freqs[i];
				g_free(element->tf_freqs);
				element->tf_freqs = tempf;
				temp = g_malloc(element->max_nfreqs * sizeof(complex double));
				for(i = 0; i < element->nfreqs; i++)
					temp[i] = element->transfer_function[i];
				g_free(element->transfer_function);
				element->transfer_function = temp;
				tempf = g_malloc(element->max_nfreqs * sizeof(double));
				for(i = 0; i < element->nfreqs; i++)
					tempf[i] = element->tf_uncertainty[i];
				g_free(element->tf_uncertainty);
				element->tf_uncertainty = tempf;
			}
			element->nfreqs++;
			element->tf_freqs[element->nfreqs - 1] = element->frequency;
		}

		complex double *new_data = (complex double *) mapinfo.data;
		for(i = 0; i < mapinfo.size / element->unit_size; i++) {
			element->numerator_data[element->ndata + i] = new_data[2 * i];
			element->denominator_data[element->ndata + i] = new_data[2 * i + 1];
		}
		element->ndata += mapinfo.size / element->unit_size;
	}

	/* If the frequency remains zero for more than 10 seconds after a sweep, assume the sweep is over */
	if(element->frequency == 0 && element->nfreqs > 0) {
		element->tzero += (double) GST_BUFFER_DURATION(buffer) / GST_SECOND;
		if(element->tzero > 10) {
			/* Let other elements know about the update */
			g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_TF_FREQS]);
			g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_TRANSFER_FUNCTION]);
			g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_TF_UNCERTAINTY]);

			/* Write to file if requested */
			if(element->filename != NULL)
				write_tf(element->tf_freqs, element->transfer_function, element->tf_uncertainty, element->nfreqs, element->filename);

			/* Reset for the next sweep */
			element->nfreqs = 0;
			element->ndata = 0;
			element->tzero = 0;
		}
	} else
		element->tzero = 0.0;

	gst_buffer_unmap(buffer, &mapinfo);

	return result;
}


/*
 * stop()
 */


static gboolean stop(GstBaseSink *sink) {

	GSTLALSweep *element = GSTLAL_SWEEP(sink);

	if(element->numerator_data) {
		g_free(element->numerator_data);
		element->numerator_data = NULL;
	}
	if(element->denominator_data) {
		g_free(element->denominator_data);
		element->denominator_data = NULL;
	}
	if(element->tf_freqs) {
		g_free(element->tf_freqs);
		element->tf_freqs = NULL;
	}
	if(element->transfer_function) {
		g_free(element->transfer_function);
		element->transfer_function = NULL;
	}
	if(element->tf_uncertainty) {
		g_free(element->tf_uncertainty);
		element->tf_uncertainty = NULL;
	}

	return TRUE;
}


/*
 * ============================================================================
 *
 *			      GObject Methods
 *
 * ============================================================================
 */


/*
 * properties
 */


static void set_property(GObject *object, enum property id, const GValue *value, GParamSpec *pspec) {

	GSTLALSweep *element = GSTLAL_SWEEP(object);

	GST_OBJECT_LOCK(element);

	switch(id) {
	case ARG_FREQUENCY:
		element->frequency = g_value_get_double(value);
		break;

	case ARG_FILENAME:
		element->filename = g_value_dup_string(value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


static void get_property(GObject *object, enum property id, GValue *value, GParamSpec *pspec) {

	GSTLALSweep *element = GSTLAL_SWEEP(object);

	GST_OBJECT_LOCK(element);

	switch(id) {
	case ARG_FREQUENCY:
		g_value_set_double(value, element->frequency);
		break;

	case ARG_TF_FREQS: ;
		GValue tf_freqs = G_VALUE_INIT;
		g_value_init(&tf_freqs, GST_TYPE_ARRAY);
		if(element->tf_freqs) {
			guint m;
			for(m = 0; m < element->nfreqs; m++) {
				GValue tf_freq = G_VALUE_INIT;
				g_value_init(&tf_freq, G_TYPE_DOUBLE);
				g_value_set_double(&tf_freq, element->tf_freqs[m]);
				gst_value_array_append_value(&tf_freqs, &tf_freq);
				g_value_unset(&tf_freq);
			}
		}
		g_value_copy(&tf_freqs, value);
		g_value_unset(&tf_freqs);

		break;

	case ARG_TRANSFER_FUNCTION: ;
		GValue tf = G_VALUE_INIT;
		g_value_init(&tf, GST_TYPE_ARRAY);
		if(element->transfer_function) {
			double *double_tf = (double *) element->transfer_function;
			guint n;
			for(n = 0; n < 2 * element->nfreqs; n++) {
				GValue tf_sample = G_VALUE_INIT;
				g_value_init(&tf_sample, G_TYPE_DOUBLE);
				g_value_set_double(&tf_sample, double_tf[n]);
				gst_value_array_append_value(&tf, &tf_sample);
				g_value_unset(&tf_sample);
			}
		}
		g_value_copy(&tf, value);
		g_value_unset(&tf);

		break;

	case ARG_TF_UNCERTAINTY: ;
		GValue tf_uncertainty = G_VALUE_INIT;
		g_value_init(&tf_uncertainty, GST_TYPE_ARRAY);
		if(element->tf_uncertainty) {
			guint l;
			for(l = 0; l < element->nfreqs; l++) {
				GValue tf_freq = G_VALUE_INIT;
				g_value_init(&tf_freq, G_TYPE_DOUBLE);
				g_value_set_double(&tf_freq, element->tf_uncertainty[l]);
				gst_value_array_append_value(&tf_uncertainty, &tf_freq);
				g_value_unset(&tf_freq);
			}
		}
		g_value_copy(&tf_uncertainty, value);
		g_value_unset(&tf_uncertainty);

		break;

	case ARG_FILENAME:
		g_value_set_string(value, element->filename);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


/*
 * class_init()
 */


#define CAPS \
	"audio/x-raw, " \
	"rate = (int) [1, MAX], " \
	"channels = (int) 2, " \
	"format = (string) {"GST_AUDIO_NE(Z128)"}, " \
	"layout = (string) interleaved, " \
	"channel-mask = (bitmask) 0"


static void gstlal_sweep_class_init(GSTLALSweepClass *klass) {

	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
	GstBaseSinkClass *gstbasesink_class = GST_BASE_SINK_CLASS(klass);

	gstbasesink_class->set_caps = GST_DEBUG_FUNCPTR(set_caps);
	gstbasesink_class->render = GST_DEBUG_FUNCPTR(render);
	gstbasesink_class->stop = GST_DEBUG_FUNCPTR(stop);

	gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
	gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);

	gst_element_class_set_details_simple(
		element_class,
		"Process a swept-sine injection",
		"Sink",
		"Compute a transfer function between two channels from a swept-sine injection.\n\t\t\t   "
		"The first channel is the numerator and the second is the denominator.  Both\n\t\t\t   "
		"input channels are assumed to have been mixed with a local oscillator at the\n\t\t\t   "
		"(time-varying) injection frequency.  The element reads this frequency through\n\t\t\t   "
		"the controllable property 'frequency', which is not known a priori.  It is\n\t\t\t   "
		"assumed that the injection frequency increases with time, and that the injection\n\t\t\t   "
		"runs nearly continuously (no more than a 5-second break) until it is done.",
		"Aaron Viets <aaron.viets@ligo.org>"
	);

	gst_element_class_add_pad_template(
		element_class,
		gst_pad_template_new(
			"sink",
			GST_PAD_SINK,
			GST_PAD_ALWAYS,
			gst_caps_from_string(CAPS)
		)
	);


	properties[ARG_FREQUENCY] = g_param_spec_double(
		"frequency",
		"Frequency",
		"The current injection frequency.  When there is no injection, this must be\n\t\t\t"
		"set to zero.",
		0.0, G_MAXDOUBLE, 0.0,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT | GST_PARAM_CONTROLLABLE
	);
	properties[ARG_TF_FREQS] = gst_param_spec_array(
		"tf-freqs",
		"Transfer Function Frequencies",
		"The frequencies at which the transfer function is computed.  After a transfer\n\t\t\t"
		"function is completed, this is cleared as the next set of frequencies may\n\t\t\t"
		"differ.  If injection frequencies are not done in order, this element will\n\t\t\t"
		"not re-order them.",
		g_param_spec_double(
			"freq",
			"Frequency",
			"A single frequency in tf-freqs",
			0.0, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TRANSFER_FUNCTION] = gst_param_spec_array(
		"transfer-function",
		"Transfer Function",
		"The transfer function computed at the frequencies 'tf-freqs', stored as an\n\t\t\t"
		"array of doubles.  Even indices in the array represent real parts of the\n\t\t\t"
		"transfer function, and odd indices represent imaginary parts.",
		g_param_spec_double(
			"value",
			"Value",
			"A transfer function value (real or imaginary) at some frequency",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_TF_UNCERTAINTY] = gst_param_spec_array(
		"tf-uncertainty",
		"Transfer Function Uncertainty",
		"The relative uncertainty of the computed transfer function",
		g_param_spec_double(
			"unc",
			"Uncertainty",
			"A single value of uncertainty in the array",
			0.0, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_FILENAME] = g_param_spec_string(
		"filename",
		"Filename",
		"Name of file in which to write transfer functions. If not given, no file\n\t\t\t"
		"is produced.",
		NULL,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);


	g_object_class_install_property(
		gobject_class,
		ARG_FREQUENCY,
		properties[ARG_FREQUENCY]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TF_FREQS,
		properties[ARG_TF_FREQS]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TRANSFER_FUNCTION,
		properties[ARG_TRANSFER_FUNCTION]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_TF_UNCERTAINTY,
		properties[ARG_TF_UNCERTAINTY]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FILENAME,
		properties[ARG_FILENAME]
	);
}


/*
 * init()
 */


static void gstlal_sweep_init(GSTLALSweep *element) {

	element->rate = 0;
	element->unit_size = 0;
	element->t0 = GST_CLOCK_TIME_NONE;
	element->offset0 = GST_BUFFER_OFFSET_NONE;
	element->next_in_offset = GST_BUFFER_OFFSET_NONE;
	element->nfreqs = 0;
	element->ndata = 0;
	element->tzero = 0.0;
	element->filename = NULL;
	element->numerator_data = NULL;
	element->denominator_data = NULL;
	element->tf_freqs = NULL;
	element->transfer_function = NULL;
	element->tf_uncertainty = NULL;

	gst_base_sink_set_sync(GST_BASE_SINK(element), FALSE);
	gst_base_sink_set_async_enabled(GST_BASE_SINK(element), FALSE);
}

