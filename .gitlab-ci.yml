image: docker:latest

include:
  # https://computing.docs.ligo.org/gitlab-ci-templates/
  - project: computing/gitlab-ci-templates
    file:
      # https://computing.docs.ligo.org/gitlab-ci-templates/python/
      - python.yml
      # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/
      - rhel.yml

variables:
  DOCKER_DRIVER: overlay
  DOCKER_BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  DOCKER_LATEST: $CI_REGISTRY_IMAGE:latest
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

  # Location of *.spec.in patch script and arguments:
  PATCHSCRIPT: patch_optimized_spec_file
  COMP: gcc
  PATCHFLAGS: -c $COMP -k --nocheck -f

  # don't need git history
  GIT_DEPTH: 1

cache:
  key: $CI_JOB_NAME
  paths:
    - ccache

stages:
  - distribution
  - source packages
  - build
  - test
  - docker
  - docker-latest
  - lint

# Build distribution
dist:
  stage: distribution
  interruptible: true
  image: containers.ligo.org/lscsoft/gstlal:gstlal-ugly-1.10.0-v1
  script:
    - ./00init.sh
    - ./configure --enable-gtk-doc $EXTRA_CONFIG_FLAGS
    - make dist
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_PIPELINE_SOURCE == "push"
      when: always
  artifacts:
    paths:
      - gstlal-calibration-*.tar.*

# Build srpm
srpm:
  # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:srpm
  extends: .rhel:srpm
  stage: source packages
  interruptible: true
  needs:
    - dist
  variables:
    TARBALL: "gstlal-calibration-*.tar.*"
  image: igwn/builder:el8-testing
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_PIPELINE_SOURCE == "push"
      when: always

# Build rpms
build:
  # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:rpm
  extends: .rhel:rpm
  stage: build
  interruptible: true
  image: igwn/builder:el8-testing
  variables:
    SRPM: "gstlal-calibration-*.src.rpm"
    RPM_BUILD_CPUS: 4
  needs:
    - srpm
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_PIPELINE_SOURCE == "push"
      when: always

# Lint rpms
rpmlint:
  # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:lint
  extends: .rhel:lint
  stage: lint
  image: igwn/base:el8-testing
  interruptible: true
  needs:
    - build
  before_script:
    - !reference [".rhel:lint", "before_script"]
    # install our rpms
    - dnf -y -q install *.rpm
  script:
    # lint the built rpms _and_ the installed ones (for extra checks)
    - rpmlint *.rpm "*gstlal-calibration*"
  allow_failure: true

# Tests
test:
  # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:base
  extends: .rhel:base
  interruptible: true
  image: igwn/base:el8-testing
  stage: test
  needs:
    - build
  tags:
    - cit
  before_script:
    - !reference [".rhel:base", "before_script"]
    # build a yum repo from the upstream packages
    # so that we can install them by name, rather than file path
    - dnf install -y -q createrepo
    - LOCAL_REPO="${CI_PROJECT_DIR}/local"
    - mkdir -p ${LOCAL_REPO}
    - cp -r *.rpm ${LOCAL_REPO}
    - createrepo --quiet --workers 1 "${LOCAL_REPO}"
    - |
      cat > /etc/yum.repos.d/local.repo <<EOF
      [local]
      name=Local builds
      baseurl=file://${LOCAL_REPO}
      enabled=1
      gpgcheck=0
      EOF
    # install test requirements
    - dnf -y -q install python3-pytest
    - dnf -y -q install framel
    - dnf -y -q install git
  script:
    - export GST_DEBUG_DUMP_DOT_DIR=${CI_PROJECT_DIR}/tests/
    # install our packages
    - dnf -y -q install gstlal-calibration-*.rpm
    # check the GST plugins
    - gst-inspect-1.0
    - gst-inspect-1.0 gstlalcalibration
    # run the tests
    - python3 -m pytest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_PIPELINE_SOURCE == "push"
      when: manual
      allow_failure: true
  artifacts:
    when: always
    paths:
      - tests/tests_pytest/*.txt
      - tests/tests_pytest/*.png
      - tests/tests_pytest/ASD_data/*.png
      - tests/tests_pytest/ASD_data/*asd.txt
      - tests/tests_pytest/TDCF_data/*.png
      - tests/tests_pytest/TDCF_data/*Approx.txt
      - tests/tests_pytest/TDCF_data/*Exact.txt
      - tests/tests_pytest/pcal_data/*.png
      - tests/tests_pytest/pcal_data/*Pcal_0*.txt
      - tests/tests_pytest/pcal_data/*Pcal_1*.txt
      - tests/tests_pytest/act_data/*.png
      - tests/tests_pytest/act_data/*exc_0*.txt
      - tests/tests_pytest/act_data/*exc_1*.txt
      - tests/tests_pytest/State_Vector_data/*.png
      - tests/tests_pytest/State_Vector_data/State_Vector_Approx.txt
      - tests/tests_pytest/State_Vector_data/State_Vector_Exact.txt
    expire_in: 24h

# Docker Images

docker:rl8:
  interruptible: true
  stage: docker
  before_script: [ ]
  script:
    # add RPMs to directory to pass to docker
    - mkdir rpms

    # Copy rpms to new container.
    - mv *.rpm rpms

    # Build the container:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $DOCKER_BRANCH --file .gitlab/ci/Dockerfile.dev .
    - docker push $DOCKER_BRANCH
  needs:
    - build
    - test
  only:
    - schedules
    - pushes
  except:
    - /gstlal-([a-z]+-|)[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

docker-release:rl8:
  interruptible: true
  stage: docker
  before_script: [ ]
  script:
    # add RPMs to directory to pass to docker
    - mkdir rpms

    # Copy rpms to new container.
    - mv *.rpm rpms

    # Clear out the old rpmbuild directory
    - rm -rf rpmbuild*

    # Build the container:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $DOCKER_BRANCH --file .gitlab/ci/Dockerfile .
    - docker push $DOCKER_BRANCH
  needs:
    - build
    - test
  only:
    - /gstlal-calibration-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

latest_image:
  interruptible: true
  stage: docker-latest
  before_script: [ ]
  needs:
    - docker-release:rl8
  only:
    - /gstlal-calibration-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker tag $DOCKER_BRANCH $DOCKER_LATEST
    - docker push $DOCKER_LATEST
  retry:
    max: 2

# -- lint

flake8:
  stage: lint
  # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:flake8
  extends: .python:flake8
  needs: []
  variables:
    # don't fail the pipeline because of linting issues,
    # these are presented in the code-quality box in the
    # merge_request UI
    FLAKE8_OPTIONS: "--exit-zero"
