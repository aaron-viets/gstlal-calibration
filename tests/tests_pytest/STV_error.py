# Copyright (C) 2024  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import numpy as np

def check_bits(filename):
	# Numbers corresponding to each bit
	bits = []
	for i in range(20):
		bits.append(2**i)

	# Bit definitions
	bitnames = []
	bitnames.append("hoft_ok_bit")
	bitnames.append("obs_intent_bit")
	bitnames.append("lownoise_bit")
	bitnames.append("filters_ok_bit")
	bitnames.append("no_gap_bit")
	bitnames.append("no_stoch_inj_bit")
	bitnames.append("no_cbc_inj_bit")
	bitnames.append("no_burst_inj_bit")
	bitnames.append("no_detchar_inj_bit")
	bitnames.append("undisturbed_ok_bit")
	bitnames.append("ktst_smooth_bit")
	bitnames.append("kpum_smooth_bit")
	bitnames.append("kuim_smooth_bit")
	bitnames.append("kc_smooth_bit")
	bitnames.append("fcc_smooth_bit")
	bitnames.append("fs_smooth_bit")
	bitnames.append("fs_over_Q_smooth_bit")
	bitnames.append("line_sub_bit")
	bitnames.append("noise_sub_bit")
	bitnames.append("noise_sub_gate_bit")
	#bitnames.append("nonsens_sub_bit")

	standard = np.loadtxt('tests/tests_pytest/State_Vector_data/%s_standard.txt' % filename)
	data = np.loadtxt('tests/tests_pytest/State_Vector_data/%s.txt' % filename)
	# Remove the last 40 s of data, which are subject to end of stream effects
	data = data[: -40 * 16]

	# Trim if needed
	missing_initial_data_samples = int(16 * (data[0][0] - standard[0][0]))
	missing_final_data_samples = int(16 * (standard[-1][0] - data[-1][0]))
	if missing_initial_data_samples > 0:
		# Trim beginning of "standard" data
		standard = standard[missing_initial_data_samples:]
	elif missing_initial_data_samples < 0:
		# Trim beginning of test data
		data = data[abs(missing_initial_data_samples):]
	if missing_final_data_samples > 0:
		# Trim end of "standard" data
		standard = standard[:-missing_final_data_samples]
	elif missing_final_data_samples < 0:
		# Trim end of test data
		data = data[:missing_final_data_samples]

	standard = np.transpose(standard)[1]
	data = np.transpose(data)[1]

	# Make sure there is enough data
	assert len(standard) == len(data), "mismatched length of data in test and standard data for state vector"
	assert len(data) > 15 * 60 * 16, "Not enough data for a reliable test" # ~15-minute minimum requirement

	for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 19]:
		# Require agreement at each sample
		e_file = open('tests/tests_pytest/error_results.txt', 'a')
		e_file.write("Testing %s %s\n" % (filename, bitnames[i]))
		for j in range(len(data)):
			assert int(data[j]) & bits[i] == int(standard[j]) & bits[i], "%s failure!" % bitnames[i]
		e_file.write("%s passed\n" % bitnames[i])
		e_file.close()
	for i in [0, 10, 11, 12, 13, 14, 15, 16]:
		# Allow small differences in on/off transition times
		e_file = open('tests/tests_pytest/error_results.txt', 'a')
		e_file.write("Testing %s %s\n" % (filename, bitnames[i]))
		consecutive_failures = 0
		for j in range(len(data)):
			if int(data[j]) & bits[i] != int(standard[j]) & bits[i]:
				consecutive_failures += 1
			else:
				consecutive_failures = 0
			assert consecutive_failures < 160, "%s failure!" % bitnames[i] # 10 seconds at a 16-Hz sample rate
		e_file.write("%s passed\n" % bitnames[i])
		e_file.close()
	for i in [17, 18]:
		# Require agreement at a majority of samples
		e_file = open('tests/tests_pytest/error_results.txt', 'a')
		e_file.write("Testing %s %s\n" % (filename, bitnames[i]))
		failures = 0
		for j in range(len(data)):
			if int(data[j]) & bits[i] != int(standard[j]) & bits[i]:
				failures += 1
		assert float(failures) / len(data) < 0.1, "%s failure!" % bitnames[i]
		e_file.write("%s passed\n" % bitnames[i])
		e_file.close()

