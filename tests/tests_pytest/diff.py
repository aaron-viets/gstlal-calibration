# Copyright (C) 2023  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
import sys

def Diff():
    strain_success = 0
    nolines_success = 0
    clean_success = 0
    frames = 0

    start = int(os.popen("ls tests/tests_pytest/frames/GDS/H-H1GDS_LaterStart-*-4.gwf").read().split("\n")[0].split('-')[2])
    start += 1332 # Settle time
    end = int(os.popen("ls tests/tests_pytest/frames/GDS/H-H1GDS_LaterStart-*-4.gwf").read().split("\n")[-2].split('-')[2])
    assert end >= start, "Not enough input data to do FrDiff test.  Need at least %d seconds more" % (end - start)
    for i in range(start, end, 4):
        frames += 1
        file1 = 'tests/tests_pytest/frames/GDS/H-H1GDS_Approx-' + str(i) + '-4.gwf'
        file2 = 'tests/tests_pytest/frames/GDS/H-H1GDS_LaterStart-' + str(i) + '-4.gwf'
        strain_str = 'FrDiff -i1 {} -i2 {} -t H1:GDS-CALIB_STRAIN'.format(file1,file2)
        strain_check = os.system(strain_str)
        if strain_check == 0:
            strain_success += 1
        nolines_str = 'FrDiff -i1 {} -i2 {} -t H1:GDS-CALIB_STRAIN_NOLINES'.format(file1,file2)
        nolines_check = os.system(nolines_str)
        if nolines_check == 0:
            nolines_success += 1
        clean_str = 'FrDiff -i1 {} -i2 {} -t H1:GDS-CALIB_STRAIN_CLEAN'.format(file1,file2)
        clean_check = os.system(clean_str)
        if clean_check == 0:
            clean_success += 1
    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    e_file.write('%d frames tested with FrDiff\n%d sucesses in GDS-CALIB_STRAIN\n%d successes in GDS-CALIB_STRAIN_NOLINES\n%d successes in GDS-CALIB_STRAIN_CLEAN\n' % (frames, strain_success, nolines_success, clean_success))
    e_file.close()
    print("GDS-CALIB_STRAIN differs in {} of {} frames!".format((frames - strain_success), frames), file = sys.stderr)
    print("GDS-CALIB_STRAIN_NOLINES differs in {} of {} frames!".format((frames - nolines_success), frames), file = sys.stderr)
    print("GDS-CALIB_STRAIN_CLEAN differs in {} of {} frames!".format((frames - clean_success), frames), file = sys.stderr)
    assert strain_success == frames
    assert nolines_success == frames
    assert clean_success == frames


