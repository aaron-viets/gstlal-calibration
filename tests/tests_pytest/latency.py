# Copyright (C) 2024  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import numpy as np
import sys
from tests.tests_pytest.plot import plot_latency


def Latency():
    inp = np.loadtxt('tests/tests_pytest/input_unix_timestamps.txt')
    outp = np.loadtxt('tests/tests_pytest/output_unix_timestamps.txt')

    inp = np.transpose(inp)
    outp = np.transpose(outp)
    
    i_real = inp[1]
    o_real = outp[1]
    i_gps = inp[0]
    o_gps = outp[0]

    i_lst = [list(i_gps).index(x) for x in list(np.intersect1d(i_gps,o_gps))]   #sub a and b for left and right collumn of output, returns a list of the indexs of a to keep 
    lat = o_real - i_real[i_lst]

    # Allow 200 s for pipeline startup processes to settle, and remove the last frame, which may be a partial frame
    settled_lat = lat[50:-1]
    # Check for an increasing trend
    var = settled_lat - np.mean(settled_lat)
    # Take a running 20th percentile to reduce the impact of outliers
    percentile_length = 90 # 6 minutes
    percentile_var = np.zeros(len(var) - percentile_length + 1)
    for i in range(len(percentile_var)):
        percentile_var[i] = np.percentile(var[i : i + percentile_length], 20)
    # lat_increase estimates the latency increase in seconds from start to finish
    lat_increase = 0.0
    for i in range(len(percentile_var)):
        lat_increase += percentile_var[i] * 12 * (i - (len(percentile_var) - 1) / 2.0) / len(percentile_var) / len(percentile_var)

    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    e_file.write("Mean latency = %fs\n" % np.mean(settled_lat))
    e_file.write("Max latency = %fs\n" % max(settled_lat))
    e_file.write("%d of %d frames with latency over 6s\n" % (sum(i > 6 for i in settled_lat), len(settled_lat)))
    e_file.write("Latency increased by %fs over %fs of data\n" % (lat_increase, 4 * (len(percentile_var) - 1)))
    e_file.close()

    print("Mean latency = {}s".format(np.mean(settled_lat)), file = sys.stderr)
    print("Max latency = {}s".format(max(settled_lat)), file = sys.stderr)
    print("{} of {} frames with latency over 6s".format(sum(i > 6 for i in settled_lat), len(settled_lat)), file = sys.stderr)
    print("Latency increased by {}s over {}s of data".format(lat_increase, 4 * (len(percentile_var) - 1)), file = sys.stderr)

    np.savetxt("tests/tests_pytest/latency.txt", np.transpose(np.array([o_gps, lat])))
    plot_latency(o_gps, lat)

    # 4-s latency is typical in this configuration.  More than 5 s could
    # indicate a problem, while less than 3 s could mean that the CI
    # latency-testing pipeline is not running as it should be.
    assert np.mean(settled_lat) < 5, "Mean latency too large (%f s > 5 s)" % np.mean(settled_lat)
    assert np.mean(settled_lat) > 3, "Mean latency too small (%f s < 3 s)" % np.mean(settled_lat)
    # There should be no large excursions (This may occasionally fail)
    assert max(settled_lat) < 10, "Max latency too large (%f s > 10 s)" % max(settled_lat)
    assert min(settled_lat) > 0, "Min latency too small (%f s < 0 s)" % min(settled_lat)
    # There should not be numerous small excursions
    assert sum(i > 6 for i in settled_lat) < 10, "Too many latency excursions (%d frames had latency > 6 s)" % sum(i > 6 for i in settled_lat)
    assert sum(i < 2 for i in settled_lat) < 10, "Too many latency excursions (%d frames had latency < 2 s)" % sum(i < 2 for i in settled_lat)
    assert lat_increase < 0.5

