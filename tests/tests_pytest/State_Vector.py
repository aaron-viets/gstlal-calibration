# Copyright (C) 2023  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import glob

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from gstlalcalibration import test_common

from tests.tests_pytest.STV_error import check_bits
from tests.tests_pytest.plot import plot_statevector


ifo = 'H1'
Approx_txtfilename = 'State_Vector_Approx'
Exact_txtfilename = 'State_Vector_Exact'

def state_vector_Approx(pipeline, name):
	data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/GDS_Approx_frames.cache", cache_dsc_regex = ifo)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, [("H1", "GDS-CALIB_STATE_VECTOR")])))
	data = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STATE_VECTOR", ifo, 1.0)
	data = calibration_parts.caps_and_progress(pipeline, data, "audio/x-raw,format=U32LE,channels=1,channel-mask=(bitmask)0x0", "State_Vector")
	data = pipeparts.mknxydumpsink(pipeline, data, "tests/tests_pytest/State_Vector_data/%s.txt" % Approx_txtfilename)
	return pipeline

def state_vector_Exact(pipeline, name):
	data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/GDS_Approx_frames.cache", cache_dsc_regex = ifo)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, [("H1", "GDS-CALIB_STATE_VECTOR")])))
	data = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STATE_VECTOR", ifo, 1.0)
	data = calibration_parts.caps_and_progress(pipeline, data, "audio/x-raw,format=U32LE,channels=1,channel-mask=(bitmask)0x0", "State_Vector")
	data = pipeparts.mknxydumpsink(pipeline, data, "tests/tests_pytest/State_Vector_data/%s.txt" % Exact_txtfilename)
	return pipeline


def State_Vector():
	test_common.build_and_run(state_vector_Approx, "state_vector_Approx")
	test_common.build_and_run(state_vector_Exact, "state_vector_Exact")
	plot_statevector(Approx_txtfilename)
	plot_statevector(Exact_txtfilename)
	check_bits(Approx_txtfilename)
	check_bits(Exact_txtfilename)

