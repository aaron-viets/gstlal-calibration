# Copyright (C) 2023  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
from threading import Thread


gps_start_time = 1404304216
gps_end_time = 1404306216
later_start_time = 1404304617

# Enforce that these times are integer multiples of 4 since we have 4-s raw frames
gps_start_time = gps_start_time + (4 - gps_start_time % 4) % 4
gps_end_time = gps_end_time - gps_end_time % 4

gps_frame_start = gps_start_time + 32
later_frame_start = later_start_time + 32 + (4 - later_start_time % 4) % 4

expected_fnum = (gps_end_time - gps_frame_start) // 2 + (gps_end_time - later_frame_start) // 4

def Run_calib_approx_exact():
    os.system("GST_DEBUG=3 gstlal_compute_strain --gps-start-time %d --gps-end-time %d --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=4 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_Approx_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20240330T211519Z.npz" % (gps_start_time, gps_end_time))
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS_Approx-*.gwf | lal_path2cache > tests/tests_pytest/GDS_Approx_frames.cache")

    os.system("GST_DEBUG=3 gstlal_compute_strain --gps-start-time %d --gps-end-time %d --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=4 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_Exact_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20240330T211519Z.npz" % (gps_start_time, gps_end_time))
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS_Exact-*.gwf | lal_path2cache > tests/tests_pytest/GDS_Exact_frames.cache")


def Run_calib_later_start():
    os.system("GST_DEBUG=3 gstlal_compute_strain --gps-start-time %d --gps-end-time %d --frame-cache tests/tests_pytest/raw_frames.cache --output-path tests/tests_pytest/frames/GDS --frame-duration=4 --frames-per-file=1 --wings=0 --config-file tests/tests_pytest/filters/gstlal_compute_strain_ApproxLaterStart_H1.ini --filters-file-name tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20240330T211519Z.npz" % (later_start_time, gps_end_time))
    os.system("ls tests/tests_pytest/frames/GDS/H-H1GDS_LaterStart-*.gwf | lal_path2cache > tests/tests_pytest/GDS_LaterStart_frames.cache")


def Run_calib():
    os.system("ls tests/tests_pytest/frames/raw/H-H1_R-*.gwf | lal_path2cache > tests/tests_pytest/raw_frames.cache")
    os.system("mkdir -p tests/tests_pytest/frames/GDS")

    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    e_file.write('Running calibration pipelines\n')
    e_file.close()

    approx_exact_thread = Thread(target = Run_calib_approx_exact)
    later_start_thread = Thread(target = Run_calib_later_start)
    approx_exact_thread.start()
    later_start_thread.start()
    approx_exact_thread.join()
    later_start_thread.join()

    fnum = len(os.listdir('tests/tests_pytest/frames/GDS'))
    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    e_file.write('Finished running calibration pipelines\n')
    e_file.write('%d frames expected from calibration pipelines\n%d frames produced by calibration pipelines\n' % (expected_fnum, fnum))
    e_file.close()
    assert fnum == expected_fnum, "Calibration pipelines should produce %d frames but only produced %d." % (expected_fnum, fnum)

