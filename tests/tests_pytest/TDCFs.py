# Copyright (C) 2024  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import numpy

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from gstlalcalibration import test_common

from tests.tests_pytest.error import rms
from tests.tests_pytest.plot import plot_TDCFs


ifo = 'H1'
gstlal_frame_cache_list = ["tests/tests_pytest/GDS_Approx_frames.cache", "tests/tests_pytest/GDS_Exact_frames.cache"]
TDCF_names = ['KAPPA_TST_REAL', 'KAPPA_TST_IMAGINARY', 'KAPPA_PUM_REAL', 'KAPPA_PUM_IMAGINARY', 'KAPPA_UIM_REAL', 'KAPPA_UIM_IMAGINARY', 'KAPPA_C', 'F_CC', 'F_S_SQUARED', 'SRC_Q_INVERSE']
channels = []
for TDCF_name in TDCF_names:
	channels.append("GDS-CALIB_%s" % TDCF_name)


channel_list = []
for channel in channels:
	channel_list.append((ifo, channel))


#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def read_tdcfs(pipeline, name):

	# Get TDCFs from frames
	for cache in gstlal_frame_cache_list:
		version = "Approx" if "Approx" in cache else "Exact"
		# Get gstlal channels from the gstlal frames
		data = pipeparts.mklalcachesrc(pipeline, location = cache, cache_dsc_regex = ifo)
		data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
		for i in range(len(TDCF_names)):
			TDCF = calibration_parts.hook_up(pipeline, data, channels[i], ifo, 1.0, element_name_suffix = version)
			pipeparts.mknxydumpsink(pipeline, TDCF, "tests/tests_pytest/TDCF_data/%s_%s.txt" % (TDCF_names[i], version))

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


def TDCFs():
	test_common.build_and_run(read_tdcfs, "read_tdcfs")
	plot_TDCFs()
	rms('T')

