# Copyright (C) 2023  Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from tests.tests_pytest.run_calib_pipeline import Run_calib
from tests.tests_pytest.ASD import ASD
from tests.tests_pytest.TDCFs import TDCFs
from tests.tests_pytest.State_Vector import State_Vector
from tests.tests_pytest.act2darm_timeseries import Act2darm
from tests.tests_pytest.pcal2darm_timeseries import Pcal2darm
from tests.tests_pytest.diff import Diff
from tests.tests_pytest.latency import Latency


def test_Calib_Pipeline():
    Run_calib()

def test_ASD():
    ASD()

def test_TDCFs():
    TDCFs()

def test_state_vector():
    State_Vector()

def test_act2darm():
    Act2darm()

def test_pcal2darm():
    Pcal2darm()

def test_diff():
    Diff()

def test_latency():
    Latency()

