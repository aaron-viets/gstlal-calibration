#!/usr/bin/env python3
# Copyright (C) 2024  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import numpy as np
import os
import hashlib
from optparse import OptionParser, Option


parser = OptionParser()
parser.add_option("--input", type = str, help = "A calibrated frame file, a filters file, a hash (either full or first 7 characters), or an integer.  The program attempts to find a filters file with the same sha256 checksum as the input.")
parser.add_option("--primary-directory", type = str, default = "/home/cal/archive/", help = "Path to directory where program should begin searching")
parser.add_option("--no-home-directory", action = "store_true", help = "If set, the program will not check your home directory.")

options, filenames = parser.parse_args()
inp = options.input
primary_dir = options.primary_directory


#
# Function definition to get sha256 checksum of a file
#

def file_sha256(path):
	with open(path, 'rb') as f:
		hexsha = hashlib.sha256(f.read()).hexdigest()
	return hexsha


hash_hex = None
if inp.endswith(".gwf"):
	# Then the input is a frame file
	frchannels = os.popen("FrChannels %s" % inp).read().split('\n')
	chan_num = 0
	found_channel = False
	for i in range(len(frchannels)):
		if "CALIB_REPORT_GDS_HASH_INT" in frchannels[i]:
			found_channel = True
			chan_num = i
	if not found_channel:
		raise ValueError("channel CALIB_REPORT_GDS_HASH_INT is not present in the input frame %s" % inp)
	frdump = os.popen("FrDump -i %s -d 4 -t %s" % (inp, frchannels[chan_num].split(" ")[0])).read()
	hash_int = int(frdump.split("mean ")[1].split(".")[0])
	hash_hex = hex(hash_int)[2:]
	while len(hash_hex) < 7:
		hash_hex = '0' + hash_hex
	if len(hash_hex) > 7:
		print("Warning: unexpected hash length %d from frame.  Expected length is 7." % len(hash_hex))


elif inp.endswith(".npz"):
	# Then the input is a filters file
	hash_hex = file_sha256(inp)

elif inp.isnumeric():
	# Then the input is an int corresponding to a 7-character hash
	hash_int = int(inp)
	hash_hex = hex(hash_int)[2:]
	while len(hash_hex) < 7:
		hash_hex = '0' + hash_hex
	if len(hash_hex) > 7:
		print("Warning: unexpected hash length %d from input integer.  Expected length is 7." % len(hash_hex))

else:
	# The input is a hash
	hash_hex = inp
	if len(hash_hex) < 7:
		print("Warning: too few characters in hash.  This may produce a result that is not unique")

if hash_hex is None:
	raise ValueError("Unable to find hash")


# We need to search for filters files with the needed sha256 checksum
filters_paths = []
print("Searching for filters files with sha256 checksum %s ..." % hash_hex)
# Check the user's home directory
if not options.no_home_directory and os.environ['HOME'] != primary_dir:
	for dirpath, dirs, files in os.walk(os.environ['HOME']):
		for f in files:
			if f.endswith(".npz"):
				# Check if it is a match
				if file_sha256(os.path.join(dirpath, f)).startswith(hash_hex):
					# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
					if dirpath.count("GDSFilters") > 0:
						filters_paths.insert(0, os.path.join(dirpath, f))
					else:
						filters_paths.append(os.path.join(dirpath, f))

# Check the intended primary directory
for dirpath, dirs, files in os.walk(primary_dir):
	for f in files:
		if f.endswith(".npz"):
			# Check if it is a match
			if file_sha256(os.path.join(dirpath, f)).startswith(hash_hex):
				filters_paths.insert(0, os.path.join(dirpath, f))

if not len(filters_paths):
	raise ValueError("Cannot find filters file with sha256 checksum %s in %s or in home directory %s", (hash_hex, primary_dir, os.environ['HOME']))

if len(filters_paths) == 1:
	print("Found 1 filters file that matches:")
else:
	print("Found %d filters files that match:" % (len(filters_paths)))
for filters_path in filters_paths:
	print("%s" % filters_path)




