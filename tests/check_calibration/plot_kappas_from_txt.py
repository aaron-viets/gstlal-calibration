#!/usr/bin/env python3
# Copyright (C) 2020  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy as np
import time
from math import pi
import resource
import datetime
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 32
matplotlib.rcParams['legend.fontsize'] = 20
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt
from ticks_and_grid import ticks_and_grid

from optparse import OptionParser, Option

from lal import LIGOTimeGPS

parser = OptionParser()
parser.add_option("--txt-list", metavar = "list", help = "colon-, semicolon-, and comma-separated list of txt files to read in.  Commas separate different datasets from the same calibration type to be placed on the same plot.  Semicolons separate different calibration types to be placed on the same plot.  Colons separate datasets to be placed on different plots arranged vertically on the same figure.")
parser.add_option("--labels", metavar = "list", help = "Semicolon-separated list of names to use in plot legends for different calibrated data sets")
parser.add_option("--plot-title", metavar = "name", default = None, help = "Title to appear at the top of the figure")
parser.add_option("--filename", metavar = "name", type = str, default = "", help = "Name of output file.  GPS times will be added at the end of the filename.")

options, filenames = parser.parse_args()

# Set up list of labels to be used in plot legends and filenames
short_labels = options.labels.split(';')

# Arrange list of data sets
t_start = np.inf
t_end = 0
data = []
labels = []
ylabels = []
data_min = []
data_max = []
txt_list = options.txt_list.split(':')
for i in range(len(txt_list)):
	data.append([])
	labels.append([])
	ylabels.append([])
	data_min.append([])
	data_max.append([])
	txt_list[i] = txt_list[i].split('|')
	for j in range(len(txt_list[i])):
		ylabels[i].append(None)
		data[i].append([])
		labels[i].append([])
		txt_list[i][j] = txt_list[i][j].split(';')
		if len(txt_list[i][j]) != len(short_labels):
			raise ValueError("Number of lables must match number of versions of calibration. %d != %d" % (len(short_labels), len(txt_list[i][j])))
		for k in range(len(txt_list[i][j])):
			data[i][j].append([])
			labels[i][j].append([])
			txt_list[i][j][k] = txt_list[i][j][k].split(',')
			for l in range(len(txt_list[i][j][k])):
				data[i][j][k].append(np.loadtxt(txt_list[i][j][k][l]).transpose())
				labels[i][j][k].append(short_labels[k])
				t_start = min(t_start, data[i][j][k][l][0][0])
				t_end = max(t_end, data[i][j][k][l][0][-1])
				# Find out which TDCF this is and add that to the label
				if 'tst' in txt_list[i][j][k][l] or 'TST' in txt_list[i][j][k][l]:
					if 'imag' in txt_list[i][j][k][l] or 'IMAG' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Im(\kappa_{\rm T})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(-0.1)
							data_max[i].append(0.1)
					elif 'real' in txt_list[i][j][k][l] or 'REAL' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Re(\kappa_{\rm T})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
					elif 'tau' in txt_list[i][j][k][l] or 'TAU' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\tau_{\rm T}$'
						ylabels[i][j] = "Time ($\mu$s)"
						if k == 0 and l == 0:
							data_min[i].append(-1000)
							data_max[i].append(1000)
					else:
						# Assume it's the magnitude
						labels[i][j][k][l] += r': $\kappa_{\rm T}$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
				elif 'pum' in txt_list[i][j][k][l] or 'PUM' in txt_list[i][j][k][l]:
					if 'imag' in txt_list[i][j][k][l] or 'IMAG' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Im(\kappa_{\rm P})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(-0.1)
							data_max[i].append(0.1)
					elif 'real' in txt_list[i][j][k][l] or 'REAL' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Re(\kappa_{\rm P})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
					elif 'tau' in txt_list[i][j][k][l] or 'TAU' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\tau_{\rm P}$'
						ylabels[i][j] = "Time ($\mu$s)"
						if k == 0 and l == 0:
							data_min[i].append(-1000)
							data_max[i].append(1000)
					else:
						# Assume it's the magnitude
						labels[i][j][k][l] += r': $\kappa_{\rm P}$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
				elif 'uim' in txt_list[i][j][k][l] or 'UIM' in txt_list[i][j][k][l]:
					if 'imag' in txt_list[i][j][k][l] or 'IMAG' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Im(\kappa_{\rm U})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(-0.1)
							data_max[i].append(0.1)
					elif 'real' in txt_list[i][j][k][l] or 'REAL' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Re(\kappa_{\rm U})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
					elif 'tau' in txt_list[i][j][k][l] or 'TAU' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\tau_{\rm U}$'
						ylabels[i][j] = "Time ($\mu$s)"
						if k == 0 and l == 0:
							data_min[i].append(-1000)
							data_max[i].append(1000)
					else:
						# Assume it's the magnitude
						labels[i][j][k][l] += r': $\kappa_{\rm U}$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
				elif 'pu' in txt_list[i][j][k][l] or 'PU' in txt_list[i][j][k][l]:
					if 'imag' in txt_list[i][j][k][l] or 'IMAG' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Im(\kappa_{\rm PU})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(-0.1)
							data_max[i].append(0.1)
					elif 'real' in txt_list[i][j][k][l] or 'REAL' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\Re(\kappa_{\rm PU})$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
					elif 'tau' in txt_list[i][j][k][l] or 'TAU' in txt_list[i][j][k][l]:
						labels[i][j][k][l] += r': $\tau_{\rm PU}$'
						ylabels[i][j] = "Time ($\mu$s)"
						if k == 0 and l == 0:
							data_min[i].append(-1000)
							data_max[i].append(1000)
					else:
						# Assume it's the magnitude
						labels[i][j][k][l] += r': $\kappa_{\rm PU}$'
						ylabels[i][j] = "Correction"
						if k == 0 and l == 0:
							data_min[i].append(0.9)
							data_max[i].append(1.1)
				elif 'kc' in txt_list[i][j][k][l] or 'KC' in txt_list[i][j][k][l] or 'kappac' in txt_list[i][j][k][l] or 'KAPPAC' in txt_list[i][j][k][l] or 'kappa_c' in txt_list[i][j][k][l] or 'KAPPA_C' in txt_list[i][j][k][l] or 'kappa_C' in txt_list[i][j][k][l]:
					labels[i][j][k][l] += r': $\kappa_{\rm C}$'
					ylabels[i][j] = r'$\kappa_{\rm C}$'
					if k == 0 and l == 0:
						data_min[i].append(0.95)
						data_max[i].append(1.05)
				elif 'fc' in txt_list[i][j][k][l] or 'FC' in txt_list[i][j][k][l] or 'f_c' in txt_list[i][j][k][l] or 'F_C' in txt_list[i][j][k][l]:
					labels[i][j][k][l] += r': $f_{\rm cc}$'
					ylabels[i][j] = r'$f_{\rm cc}$ (Hz)'
					if k == 0 and l == 0:
						data_min[i].append(400)
						data_max[i].append(500)
				elif ('fs_over_Q' in txt_list[i][j][k][l] or 'FS_OVER_Q' in txt_list[i][j][k][l] or 'f_s_over_Q' in txt_list[i][j][k][l] or 'F_S_OVER_Q' in txt_list[i][j][k][l]):
					labels[i][j][k][l] += r': $f_{\rm s} / Q$'
					ylabels[i][j] = r'$f_{\rm s} / Q$ (Hz)'
					if k == 0 and l == 0:
						data_min[i].append(-2.2)
						data_max[i].append(1)
				elif 'fs' in txt_list[i][j][k][l] or 'FS' in txt_list[i][j][k][l] or 'f_s' in txt_list[i][j][k][l] or 'F_S' in txt_list[i][j][k][l]:
					labels[i][j][k][l] += r': $f_{\rm s}^2$'
					ylabels[i][j] = r'$f_{\rm s}^2$ (Hz$^2$)'
					if k == 0 and l == 0:
						data_min[i].append(-20)
						data_max[i].append(300)
				elif 'Q' in txt_list[i][j][k][l] or 'q' in txt_list[i][j][k][l]:
					labels[i][j][k][l] += r': $Q^{-1}$'
					ylabels[i][j] = "Inverse Quality Factor"
					if k == 0 and l == 0:
						data_min[i].append(-1)
						data_max[i].append(1)
				else:
					labels[i][j][k][l] += 'TDCF'
					ylabels[i][j] = "Correction"
					if k == 0 and l == 0:
						data_min[i].append(min(data[i][j][k][l][1]))
						data_max[i].append(max(data[i][j][k][l][1]))
					print('unknown legend label for %s' % txt_list[i][j][k][l])

# Read data from files and plot it
colors = [['red', 'darkred', 'salmon'], ['limegreen', 'darkgreen', 'lightgreen'], ['blue', 'darkblue', 'lightblue']] # Hopefully the user will not compare more than 3 versions of calibration or plot more than 3 TDCFs on the same plot.

# Figure out the (horizontal) time axis
dur = t_end - t_start
t_unit = 'seconds'
sec_per_t_unit = 1.0
if dur > 60 * 60 * 100:
	t_unit = 'days'
	sec_per_t_unit = 60.0 * 60.0 * 24.0
elif dur > 60 * 100:
	t_unit = 'hours'
	sec_per_t_unit = 60.0 * 60.0
elif dur > 100:
	t_unit = 'minutes'
	sec_per_t_unit = 60.0

# Make plots
axs = []
plt.figure(figsize = (18, len(data) * 6))
for i in range(len(data)):
	axs = []
	num_legend = 0
	# For the color scheme, we need the indices j (which y-axis) and k (which calibration version) to have the reverse hierarchy.
	datasetnum = np.zeros(len(short_labels), dtype = int)
	for j in range(len(data[i])):
		num_legend = 0
		ax = plt.subplot(len(data), 1, i + 1) if j == 0 else ax.twinx()
		axs.append(ax)
		for k in range(len(data[i][j])):
			for l in range(len(data[i][j][k])):
				ax.plot((data[i][j][k][l][0] - t_start) / sec_per_t_unit, data[i][j][k][l][1], colors[k % 3][datasetnum[k] % 3], linewidth = 2.0, label = labels[i][j][k][l])
				plt.tight_layout()
				num_legend += 1
				datasetnum[k] += 1
		if ylabels[i][j] is not None:
			plt.ylabel(ylabels[i][j])
		ticks_and_grid(ax, xscale = 'linear', yscale = 'linear', ymin = data_min[i][j], ymax = data_max[i][j], show_grid = j == 0)
		if i == 0 and j == 0 and options.plot_title is not None:
			plt.title(options.plot_title)

		if i == len(data) - 1 and j == 0:
			plt.xlabel(r'${\rm Time \  in \  %s \  since \  %s \  UTC}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t_start + 315964782))))

	# Legends must be added last so that they don't get covered by other things.
	for j in range(len(data[i])):
		leg = axs[j].legend(fancybox = True, loc = 'upper right' if j == len(data[i]) - 1 else 'upper left', ncol = max(1, num_legend // 3))
		leg.get_frame().set_alpha(0.6)

plt.savefig(options.filename + '_%d-%d.' % (t_start, dur) + 'png')
plt.savefig(options.filename + '_%d-%d.' % (t_start, dur) + 'pdf')

