#!/usr/bin/env python3
# Copyright (C) 2022  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy as np
import time
from math import pi
import resource
import datetime
import time
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlalcalibration import test_common
from gi.repository import Gst

from ticks_and_grid import ticks_and_grid


#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#

def secular_change_01(pipeline, name):

	#
	# This test reads kappa_c from frames and attempts to detect secular changes
	#

	src = pipeparts.mklalcachesrc(pipeline, location = "H1_C00_frames.cache", cache_dsc_regex = "H1", use_mmap = True)
	src = pipeparts.mkframecppchanneldemux(pipeline, src)
	kc = calibration_parts.hook_up(pipeline, src, "GDS-CALIB_KAPPA_C", "H1", 1.0)
	kc = calibration_parts.caps_and_progress(pipeline, kc, "audio/x-raw,format=F64LE,rate=16,channels=1,bitmap=0x0", "kc")
	kc = calibration_parts.mkresample(pipeline, kc, 0, False, 1)
	kc = pipeparts.mktee(pipeline, kc)
	pipeparts.mknxydumpsink(pipeline, pipeparts.mkgeneric(pipeline, kc, "lal_insertgap", insert_gap = False, chop_length = int(1000000000 * 60 * 60 * 0)), "kc.txt")
	# Use a 2-hour variance divided by a 2-hour mean of a 10-minute variance as an indicator of secular change.
	ratio = calibration_parts.detect_secular_change(pipeline, kc, 4096, rate = 1, coherence_time = 128)
	# First 2 hours and 10 minutes of data is junk
	ratio = pipeparts.mkgeneric(pipeline, ratio, "lal_insertgap", insert_gap = False, chop_length = int(1000000000 * 60 * 60 * 0))
	pipeparts.mknxydumpsink(pipeline, ratio, "secular_change.txt")

	#
	# done
	#
	
	return pipeline


#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(secular_change_01, "secular_change_01")

kc = np.transpose(np.loadtxt("kc.txt"))
t = kc[0]
kc = kc[1]
secular_change = np.transpose(np.loadtxt("secular_change.txt"))[1]
plt.figure(figsize = (10, 10))
plt.subplot(211)
plt.plot(t, kc, "blue", linewidth = 0.75)
ticks_and_grid(plt.gca(), ymin = 0.93, ymax = 1.07, xscale = 'linear', yscale = 'linear')
plt.ylabel("kappa C")
plt.subplot(212)
plt.plot(t, secular_change, "blue", linewidth = 0.75)
ticks_and_grid(plt.gca(), ymin = 0, ymax = 12, xscale = 'linear', yscale = 'linear')
plt.ylabel("Secular Change")
plt.xlabel("GPS Time (s)")

plt.savefig("secular_change.png")


